# PruebaFront

Proyecto Angular desarrollado por Josue Verbel

## Development server

Para correr el servidor ejecutar el comando `ng serve`, podra ver la ejecucion del mismo en  `http://localhost:4200/`. 

## Instalacion
al dercargar el proyecto debe ejecurar en la carpeta principal del mismo el comando `npm i`. Cabe destacar que previamente se debe haber instalado node js, y angular cli, el proyecto consume una api la cual  esta desarrollada en laravel y podra ser descargada del siguinte link `https://bitbucket.org/josueverbel/prueba_backend`, alli mismo encontraran las recomendaciones para instalacion y configuracion
