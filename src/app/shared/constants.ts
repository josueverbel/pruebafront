import { environment } from '../../environments/environment';
import { Endpoint } from './endpoints';

export class Constant {
    public static PRODUCTION: boolean = environment.production;
    public static Endpoints = Endpoint;
    public static DEBUG = false;
    public static AUTH = {
        getToken: () => {
          return localStorage.getItem('token');
        },
        getUser: () => {
          return JSON.parse(localStorage.getItem('user'));
        },
        CLIENT_ID: '3',
        CLIENT_SECRET: 'firreQeWDRlDWUxjnGksdm1PDbEoxu17NPbbHgke',
        KEYS: {
          token: 'token',
          userData: 'user',
          urlBeforExpelling: 'urlBeforExpelling'
        }
      };
}