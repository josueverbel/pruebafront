import { environment } from '../../environments/environment';

export const Endpoint = {
HISTORYDATES : {
    BASE: environment.api.base + 'historyDates'
},
HOLIDAYS: {
    BASE: environment.api.base + 'holidays'
}

};