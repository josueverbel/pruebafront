import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IMyDpOptions } from 'mydatepicker';
import {CommonService} from './services/common.service';
import {DatesService} from './services'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public form: FormGroup;
  public submitted = false;
  public dates = [];
  public holidays = [];
  public myDatePickerOptions: IMyDpOptions = this.common.GetMyDrpOptions();
  constructor( private fb: FormBuilder, private common : CommonService, private dateService : DatesService,
   ) {

   }
  ngOnInit(): void {
   this.createForms();
   this.getDates();
    this.getHolidays();
  }

   private createForms() {
    this.form = this.fb.group({
      myDate: [null, Validators.required],
      days:['', [Validators.required, ]]
    });
}
getDates() {
  this.dateService.getDates().subscribe(
    res => {
      this.dates = res;
    }
  )
 
}

getHolidays() {
  this.dateService.getHolidays().subscribe(
    res => {
      this.holidays = res;
    }
  )
 
}
public submit() {

this.submitted = true;
    if (this.form.invalid) {
            
      return;
    }
  let enableDate = this.calculateNextEnableDay(this.form.value.myDate.date,  this.form.value.days);
   const data = {
     date: this.common.getDateFormatted(this.form.value.myDate.date), 
     days: this.form.value.days, 
     nextenabledday:   this.common.getFormatted(enableDate)
    };
  this.dateService.Save(data).subscribe(
    res => {
      console.log(res);
      this.getDates();
    }
  )
}

calculateNextEnableDay(anyDate, days) {
  console.log(anyDate);
  let formated = this.common.getDateFormatted(anyDate);
    let day =  anyDate.day > 9 ? anyDate.day + 1 : anyDate.day;
  let date = new Date(`${anyDate.year}-${anyDate.month}-${day}`);
  let dayToMillisecond = 24 * 60 * 60  * 1000 ;
 
    for (let i = 0; i < days; i++)
    {
     
      date = new Date( date.getTime() + dayToMillisecond);
          
        if (this.isWeekend(date) || this.isHoliday(date)) {
        
          i--;
       }
     
    }

    return date;
  }

  isWeekend(date : Date) {
    let weekDay = date.getDay();
   
    let dateIsWeekend = false;
    weekDay > 5  ||   weekDay < 1 ? dateIsWeekend = true : dateIsWeekend = false;
    return dateIsWeekend;
  }
  isHoliday(date: Date){
   let dateIsHoliday = false;
   
     let res =  this.holidays.filter(h => h.day == date.getDate() && h.month == date.getMonth() + 1);
     if (res.length > 0) {
       dateIsHoliday = true;
          
     }
    return dateIsHoliday;
  }
}
