import { Injectable, ChangeDetectorRef } from '@angular/core';
import {IMyDpOptions} from 'mydatepicker';
@Injectable({
  providedIn: 'root'
})
export class CommonService {
  constructor(

  ) {}

 

  public ToMyDprValue(fromDate, toDate ) {
    var fdate = new Date(Date.parse(fromDate));
    var tdate =  new Date(Date.parse(toDate));
  
    return  {
    beginDate: {
      year: fdate.getFullYear(),
      month: fdate.getMonth() + 1 ,
      day: fdate.getDate() + 1
    },
    endDate: {
      year: tdate.getFullYear(),
      month: tdate.getMonth() + 1,
      day: tdate.getDate() + 1
    }
    };
  }

  public getFormatted(date: Date) {
  
    var year = date.getFullYear().toString();
    var month = (date.getMonth() + 101).toString().substring(1);
    var day = (date.getDate() + 100).toString().substring(1);
    return year + "-" + month + "-" + day;
  }
  public getDateFormatted(date) {
    const month = date.month < 10 ? '0' + date.month : date.month;
    const day = date.day < 10 ? '0' + date.day : date.day;
    return `${date.year}-${month}-${day}`;
  }
  public getControlErrors(control) {
    let min = 0;
    let max = 0;
    if (control.errors && control.errors.minlength) {
      min = control.errors.minlength.requiredLength;
    }
    if (control.errors && control.errors.maxlength) {
      max = control.errors.requiredLength;
    }
   
    let errMsgs = {
      email: 'El texto deber ser un email valido',
      required: 'Este campo es obligatorio',
      minlength: 'Este campo debe contener al menos ' + min + ' Caracteres',
      maxlength: 'Este campo debe contener maximo ' + max + ' Caracteres',
      mustMatch: (control.errors && control.errors.mustMatch) ? control.errors.mustMatch : ''
    };

    let msg = [];
    for (let key in control.errors) {
      msg.push(errMsgs[key]);
    }
    return msg;
  }
  public GetDropDownSettings(single = false, id = 'id', name = 'name') {
    return {
      singleSelection: single,
      idField: id,
      textField: name,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 1,
      allowSearchFilter: true,
      enableCheckAll: true,
      closeDropDownOnSelection: true
    };
  }
  public GetMyDrpOptions(currentYear = 2000, currentMonth = 1, currentDate = 1) {
    const imdr: IMyDpOptions = {
      dateFormat: 'dd/mm/yyyy',
      markCurrentDay: true,
      firstDayOfWeek: 'su',
      monthLabels: {
        1: 'Ene',
        2: 'Feb',
        3: 'Mar',
        4: 'Abr',
        5: 'May',
        6: 'Jun',
        7: 'Jul',
        8: 'Ago',
        9: 'Sep',
        10: 'Oct',
        11: 'Nov',
        12: 'Dic'
      },
      dayLabels: {
        su: 'Dom',
        mo: 'Lun',
        tu: 'Mar',
        we: 'Mié',
        th: 'Jue',
        fr: 'Vie',
        sa: 'Sáb'
      },
      disableUntil: {
        year: currentYear,
        month: currentMonth,
        day: currentDate - 1
      }
    };
    return imdr;
  }

public getlinkpublicEvent() {
let user =  JSON.parse(localStorage.getItem('user'));
if (user) {
      return '/pages/events/new';
    }
return '/';
  }

  
public getlinkEventDetail() {
  let user =  JSON.parse(localStorage.getItem('user'));
  if (user) {
        return '/pages/events/view/';
      }
  return '/events/view/';
    }
  
}

