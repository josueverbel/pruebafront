import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GlobalService } from '../services/global.service';

@Injectable({
  providedIn: 'root'
})
export class ParentService {
  public base: string;
  constructor( public globalService: GlobalService) {
  
   }

   public getAll(): Observable<any> {
    return this.globalService.get(this.base).pipe(
      map(res => {
        return res;
      })
    );
  }

  public getById(id) {
    return this.globalService
      .get(this.base + '/' + id)
      .pipe(
        map(res => {
          return res;
        })
      );
  }

  public create(data) {
    return this.globalService.post(this.base, data).pipe(
      map(res => {
        return res;
      })
    );
  }
  

  public update(data) {
    return this.globalService
      .put(this.base + '/' + data.id, data)
      .pipe(
        map(res => {
          return res;
        })
      );
  }



}
