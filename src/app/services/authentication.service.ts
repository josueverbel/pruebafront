import { Injectable } from '@angular/core';
import { GlobalService } from '../services/global.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Constant } from '../shared/constants';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private globalService: GlobalService) {}

  redirectUrl: string;

  /**
   * @description realizar login
   * @returns Observable <any>
   */
  login(data): Observable<any> {
    return this.globalService
      .post(Constant.Endpoints.AUTH.LOGIN, {
        grant_type: 'password',
        email: data.email,
        password: data.password,
        remember_me: true
      })
      .pipe(
        map(res => {
          localStorage.setItem('token', res['access_token']);
          localStorage.setItem('user', JSON.stringify(res['user']));

          return res;
        })
      );
  }

  isLoggedIn() {
    if (localStorage.getItem('token') && localStorage.getItem('user')) {
      return true;
    }

    return false;
  }

  verifyToken() {
     return this.globalService.get(Constant.Endpoints.AUTH.VERIFY_TOKEN).pipe(
      map(res => {
        console.log(res);
        return res;
      })
    );
  }

  /**
   * logout
   */
  logout(): Observable<any> {
   
    return this.globalService.get(Constant.Endpoints.AUTH.LOGOUT).pipe(
      map(res => {
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        return res;
      })
    );
  }

  password_reset_request(email: string): Observable<any> {
    return this.globalService
      .post(Constant.Endpoints.AUTH.RESET_PASSWORD, { email: email })
      .pipe(
        map(res => {
             return res;
        })
      );
  }
  passwordFindToken(token) {
    return this.globalService
      .get(Constant.Endpoints.AUTH.RESET_PASSWORD_FIND + '/' + token)
      .pipe(
        map(res => {
          return res;
        })
      );
  }
  singUp(data) {
    return this.globalService
    .post(Constant.Endpoints.AUTH.SING_UP, data)
    .pipe(
      map(res => {
        return res;
      })
    );
  }
  resetPassword(data) {
    return this.globalService
    .put(Constant.Endpoints.AUTH.RESET_PASSWORD, data)
    .pipe(
      map(res => {
        return res;
      })
    );
  }
  ChangePassword(data) {
    return this.globalService
    .put(Constant.Endpoints.AUTH.CHANGE_PASSWORD, data)
    .pipe(
      map(res => {
        return res;
      })
    );
  }

  findActivate(token) {
    return this.globalService
      .get(Constant.Endpoints.AUTH.SING_UP_FIND_TOKEN + '/' + token)
      .pipe(
        map(res => {
          return res;
        })
      );
  }

  findAndActivate(token) {
    return this.globalService
      .get(Constant.Endpoints.AUTH.SING_UP_FIND_AND_ACTIVATE + '/' + token)
      .pipe(
        map(res => {
          return res;
        })
      );
  }

  singUpActivateAndChangePass(data) {
    return this.globalService
    .put(Constant.Endpoints.AUTH.SING_UP_ACTIVATE, data)
    .pipe(
      map(res => {
        return res;
      })
    );
  }
}
