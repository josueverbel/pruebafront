import { Injectable } from '@angular/core';
import { ParentService } from './parent.service';
import { GlobalService } from './global.service';
import { map } from 'rxjs/operators';
import { Constant } from "../shared/constants";
@Injectable({
  providedIn: 'root'
})


export class DatesService {
    constructor(public globalService: GlobalService) {
       
    }
    public getDates() {
      return this.globalService.get(Constant.Endpoints.HISTORYDATES.BASE).pipe(
        map(res => {
          return res;
        })
      );
    }
    public getHolidays() {
      return this.globalService.get(Constant.Endpoints.HOLIDAYS.BASE).pipe(
        map(res => {
          return res;
        })
      );
    }

    public Save(data) {
    return this.globalService.post(Constant.Endpoints.HISTORYDATES.BASE, data).pipe(
      map(res => {
        return res;
      })
    );
  }
}